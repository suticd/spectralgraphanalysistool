## Introduction
This is a package for distributed spectral analysis of graphs. It is an extension of the Apache Spark framework and is derived of its GraphX library. Analysis of large graphs is computationally costly, yet is often necessary even beyond BigData applications. Apache Spark is a framework that excels at the distributed handling of large datasets and already provides the foundation with its GraphX library. Those two assumptions led to the creation of this library, as it introduces a logical continuation to the basic operations of GraphX with spectral analysis and is, at the moment of its creation, the first open source library to offer distributed spectral analysis.

### For more information, please refer to the following papers:
#### About Apache Spark and GraphX
* Zaharia, Matei, et al. "Resilient distributed datasets: A fault-tolerant abstraction for in-memory cluster computing." Proceedings of the 9th USENIX conference on Networked Systems Design and Implementation. USENIX Association, 2012.
* Gonzalez, Joseph E., et al. "Graphx: Graph processing in a distributed dataflow framework." 11th {USENIX} Symposium on Operating Systems Design and Implementation ({OSDI} 14). 2014.

#### About spectral graph analysis
* Spielman, Daniel A. "Spectral graph theory and its applications." 48th Annual IEEE Symposium on Foundations of Computer Science (FOCS'07). IEEE, 2007.

#### About this library
* Šutić, Davor, and Ervin Varga. "Spectral Graph Analysis with Apache Spark." Proceedings of the 2018 International Conference on Mathematics and Statistics. ACM, 2018.

## Build
This library is written in Scala and is built using SBT. Its dependencies are Apache Spark, GraphX, and MlLib, which practically means that Apache Spark should be deployed prior to building this library.

## API
### adjacencyMatrix
#### Description
Calculates the adjacency matrix of the provided graph.
Each element at coordinates (i,j) of the adjacency matrix is set to 1, it there is an edge that connects the vertices i and j in the graph, and 0 otherwise.
 
#### Parameters
None

#### Returns
A CoordinateMatrix representing the adjacency matrix.

#### Example
```scala
    val edges : RDD[Edge[Double]] = sparkContext.parallelize( ... )
    val graph : Graph[Int, Double] = Graph.fromEdges(edges, 0).partitionBy(PartitionStrategy.EdgePartition2D)
    val spectralGraph = new SpectralGraph(graph)
    val adjacencyMat = spectralGraph.adjacencyMatrix()
```

### laplacianMatrix
#### Description
Calculates the Laplacian matrix of the provided graph. 
Each element at coordinates (i,j) of the adjacency matrix is set to -1, it there is an edge that connects the vertices i and j in the graph, and 0 otherwise, while the main diagonal contains the respective degrees of each vertex.

#### Parameters
None

#### Returns
A CoordinateMatrix representing the Laplacian matrix.

#### Example
```scala
    val edges : RDD[Edge[Double]] = sparkContext.parallelize( ... )
    val graph : Graph[Int, Double] = Graph.fromEdges(edges, 0).partitionBy(PartitionStrategy.EdgePartition2D)
    val spectralGraph = new SpectralGraph(graph)
    val laplacianMat = spectralGraph.laplacianMatrix()
```

### laplacianRayleighQuotient
#### Description
Calculates the Rayleigh quotient of the graph's Laplacian matrix for a given vector.

#### Parameters
* The vector, with the constraint that its length is equal to the number of vertices of the graph.

#### Returns
A Double equal to the Rayleigh quotient.

#### Example
```scala
    val edges : RDD[Edge[Double]] = sparkContext.parallelize( ... )
    val graph : Graph[Int, Double] = Graph.fromEdges(edges, 0).partitionBy(PartitionStrategy.EdgePartition2D)
    val spectralGraph = new SpectralGraph(graph)
	val vectorProbe = IndexedSeq.fill(spectralGraph.numberOfVertices)(Random.nextDouble()).toVector
    val rayleighQuotient = spectralGraph.laplacianRayleighQuotient(vectorProbe)
```

### laplacianQuadraticForm
#### Description
Calculates the quadratic form of the graph's Laplacian matrix for a given vector.

#### Parameters
* The vector, with the constraint that its length is equal to the number of vertices of the graph.

#### Returns
A Double equal to the quadratic form of the graph's Laplacian matrix.

#### Example
```scala
    val edges : RDD[Edge[Double]] = sparkContext.parallelize( ... )
    val graph : Graph[Int, Double] = Graph.fromEdges(edges, 0).partitionBy(PartitionStrategy.EdgePartition2D)
    val spectralGraph = new SpectralGraph(graph)
	val vectorProbe = IndexedSeq.fill(spectralGraph.numberOfVertices)(Random.nextDouble()).toVector
    val quadraticForm = spectralGraph.laplacianQuadraticForm(vectorProbe)
```

### spectralComparison
#### Description
Spectrally compares two graphs, which allows a partial ordered relation between graphs to be established.
One graph precedes the other if its Laplacian quadratic form is greater than the other's for every vector.
Since the relation is established for every valid vector, it is also established for randomly generated vectors.

** Given that the SpectralGraph class extends the PartialOrdering interface, its instances are usually compared with the usual operators (>, <, etc.). **

#### Parameters
* First SpectralGraph.
* Second SpectralGraph.

#### Returns
A Tuple containing the laplacian quadratic forms of both graphs.

#### Example
```scala
    val edges1 : RDD[Edge[Double]] = sparkContext.parallelize( ... )
    val graph1 : Graph[Int, Double] = Graph.fromEdges(edges1, 0).partitionBy(PartitionStrategy.EdgePartition2D)
    val spectralGraph1 = new SpectralGraph(graph1)
	
	val edges2 : RDD[Edge[Double]] = sparkContext.parallelize( ... )
    val graph2 : Graph[Int, Double] = Graph.fromEdges(edges2, 0).partitionBy(PartitionStrategy.EdgePartition2D)
    val spectralGraph2 = new SpectralGraph(graph2)
	
    val comparisonTuple = spectralGraph1.spectralComparison(spectralGraph1, spectralGraph2)
	
	if(comparisonTuple._1 < comparisonTuple._2){
	    println("The first graph spectrally precedes the second.")
	}
```

### sparsify
#### Description
Spectrally sparsifies the graph.

#### Parameters
* The initial graph.
* The epsilon parameter.
* The p parameter.

#### Returns
A sparsified graph.

#### Example
```scala
    val edges : RDD[Edge[Double]] = sparkContext.parallelize( ... )
    val graph : Graph[Int, Double] = Graph.fromEdges(edges, 0).partitionBy(PartitionStrategy.EdgePartition2D)
    val spectralGraph = new SpectralGraph(graph)
	val epsilon : Double = 0.4
	val p : Double  = 0.25
    val sparsifiedGraph = sg.sparsify(graph, epsilon, p)
```