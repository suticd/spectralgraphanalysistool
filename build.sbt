//import AssemblyKeys._

name := "SpectralGraphAnalysis"

version := "1.0"

organization := "--"

scalaVersion := "2.11.12"

libraryDependencies ++= Seq(
    // Spark dependency
    "org.apache.spark" % "spark-core_2.11" % "2.4.0" % "provided",
    "org.apache.spark" % "spark-graphx_2.11" % "2.4.0" % "provided",
    "org.apache.spark" % "spark-mllib_2.11" % "2.4.0" % "provided"
)

//assemblySettings

assemblyJarName in assembly := "spectral.jar"

assemblyOption in assembly := (assemblyOption in assembly).value.copy(includeScala = false)