package org.SpectralGraphAnalysis

import org.apache.spark.rdd.RDD
import org.apache.spark.graphx._
import org.apache.spark.mllib.linalg.distributed._
import org.apache.spark.mllib.linalg._
import scala.collection.immutable._
import scala.util._
import scala.math
import scala.reflect.ClassTag
import java.io._

/**
 * Provides operations for spectral analysis of a given [[org.apache.spark.graphx.Graph]].
 * 
 * @param graph The [[org.apache.spark.graphx.Graph]] subject to spectral analysis.
 */
class SpectralGraph[VD, ED] (graph : Graph[VD, ED]) extends PartialOrdering[SpectralGraph[VD, ED]] with java.io.Serializable {
  
  val numberOfVertices = graph.vertices.count().toInt
  val numberOfEdges = graph.edges.count().toInt
  
  /**
   * Calculates the adjacency matrix of the provided graph.
   * Each element at coordinates (i,j) of the adjacency matrix is set to 1, it there is an edge that connects the vertices i and j in the graph, and 0 otherwise.
   * 
   * @returns A [[CoordinateMatrix]] representing the adjacency matrix.
   */
  def adjacencyMatrix(): CoordinateMatrix = { 
    val matrixentries1 = graph.edges.map(edge => MatrixEntry(edge.srcId, edge.dstId, 1.0))
    val matrixentries2 = graph.edges.map(edge => MatrixEntry(edge.dstId, edge.srcId, 1.0))

    new CoordinateMatrix(matrixentries1 ++ matrixentries2, numberOfVertices, numberOfVertices)
  }
  
  /**
   * Calculates the Laplacian matrix of the provided graph. 
   * Each element at coordinates (i,j) of the adjacency matrix is set to -1, it there is an edge that connects the vertices i and j in the graph, and 0 otherwise,
   * while the main diagonal contains the respective degrees of each vertex.
   * 
   * @returns A [[CoordinateMatrix]] representing the Laplacian matrix.
   */
  def laplacianMatrix(): CoordinateMatrix = { 
    val matrixentries1 = graph.edges.map(edge => MatrixEntry(edge.srcId, edge.dstId, -1.0))
    val matrixentries2 = graph.edges.map(edge => MatrixEntry(edge.dstId, edge.srcId, -1.0))

    val degrees = vertexDegrees().map(tuple => MatrixEntry(tuple._1, tuple._1, tuple._2))
      
    new CoordinateMatrix(matrixentries1 ++ matrixentries2 ++ degrees, numberOfVertices, numberOfVertices)
  }
  
  /**
   * Calculates the Rayleigh quotient of the graph's Laplacian matrix for a given vector.
   * 
   * @param vector The vector, with the constraint that its length is equal to the number of vertices of the graph. 
   * @return The Rayleigh quotient.
   */
  def laplacianRayleighQuotient(vector : scala.collection.immutable.Vector[Double]) : Double = {
    if (vector.length != numberOfVertices) {
        sys.error("The vector's length (" + vector.length + ")" + "does not match the number of vertices (" + numberOfVertices + ").")
    }
    
    val x = laplacianQuadraticForm(vector)
    val y = vector.map(elem => elem * elem).reduce(_+_)
    x / y
  }
  
  /**
   * Calculates the quadratic form of the graph's Laplacian matrix for a given vector.
   * 
   * @param vector The vector, with the constraint that its length is equal to the number of vertices of the graph. 
   * @return The quadratic form of the graph's Laplacian matrix.
   */
  def laplacianQuadraticForm(vector : scala.collection.immutable.Vector[Double]) : Double = {
    if (vector.length != numberOfVertices) {
        sys.error("The vector's length (" + vector.length + ")" + "does not match the number of vertices (" + numberOfVertices + ").")
    }
    
    graph.edges.map(edge => (vector(edge.srcId.toInt) - vector(edge.dstId.toInt))*(vector(edge.srcId.toInt) - vector(edge.dstId.toInt))).reduce(_+_)
  }
  
  /**
   * Spectrally compares two graphs, which allows a partial ordered relation between graphs to be established.
   * One graph precedes the other if its Laplacian quadratic form is greater than the other's for every vector.
   * Since the relation is established for every valid vector, it is also established for randomly generated vectors.
   * 
   * @param spectralGraphAnalyzer The [[SpectralGraphAnalyzer]] object to compare to.
   * @return True, if this graph precedes or equals the other, false otherwise.
   */
  def spectralComparison(x: SpectralGraph[VD, ED], y: SpectralGraph[VD, ED]) : (Double, Double) = {
    if (x.numberOfVertices != y.numberOfVertices) {
      sys.error("The number of vertices in the first graph (" + x.numberOfVertices + ")" + "does not match the number of vertices in the second graph (" + y.numberOfVertices + ").")
    }
    
    val vectorProbe = IndexedSeq.fill(x.numberOfVertices)(Random.nextDouble()).toVector
    val xQuadraticForm = x.laplacianQuadraticForm(vectorProbe)
    val yQuadraticForm = y.laplacianQuadraticForm(vectorProbe)

    (xQuadraticForm, yQuadraticForm)
  }
    
  def lteq(x: SpectralGraph[VD, ED], y: SpectralGraph[VD, ED]): Boolean = {
    val laplacianQuadraticFormValues = spectralComparison(x, y)
    laplacianQuadraticFormValues._1 <= laplacianQuadraticFormValues._2
  }
  
  def tryCompare(x: SpectralGraph[VD, ED], y: SpectralGraph[VD, ED]): Option[Int] = {
    val laplacianQuadraticFormValues = spectralComparison(x, y)
    Some((laplacianQuadraticFormValues._1 - laplacianQuadraticFormValues._2).toInt)
  }
  
  /**
   * Sparsifies the given graph.
   * 
   * @param subGraph The graph for sparsification.
   * @param epsilon The epsilon parameter.
   * @param p The p parameter.
   * @return A sparsified graph.
   */
  def sparsify[VD:ClassTag, ED](subGraph : Graph[VD, Double], epsilon : Double, p : Double) : Graph[VD, Double] = {
    val b = 6 / epsilon
    val q = math.ceil(b)
    val subgraphNumberOfVertices = subGraph.vertices.count()
    val l = math.ceil(math.log(2 * b * b * b * subgraphNumberOfVertices * subgraphNumberOfVertices * subgraphNumberOfVertices) / math.log(2))
    
    // the new weight is very close to the original one, altough truncated to its most significant bits
    def newEdgeWeight(weight : Double) : Double = {
        // the goal is to chose re so that q <= 2^r_e * weight < 2q; this is achieved by solving both inequalities to obtain a lower and upper bound of re, the result is an arithmetic mean
        val lowerBound = math.log(q / weight) / math.log(2)
        val upperBound = math.log(2 * q / weight) / math.log(2)
        val re = (lowerBound + upperBound) / 2
        
        // qe is intended to be the largest integer such that q_e * 2^{-r_e} <= weight; this is achieved by solving the equation resulting from the assumed equality and rounding the result down to the nearest integer
        val qe = math.floor(math.pow(2, re) * weight).toInt
        
        // the new weight is equal to q_e * 2^{-r_e}
        qe * math.pow(2, -re)
    }
    
    // the set of edges updated with the new weights
    val newEdges = subGraph.edges.map(edge => Edge[Double](edge.srcId, edge.dstId, newEdgeWeight(edge.attr)))
    
    // since the edges contain only fractional weights, 10 significant bits are taken; this is also the number of the graphs resulting from the decomposition
    val numberOfSignificantBits = 10
    
    // join each edge with the binary representation of its weight
    val edgesWithBinaryMapping = newEdges.map(edge => (edge, getBinaryFormOfFraction(edge.attr, 10))).cache()
    
    def decompose(i : Int, resultingGraphSet : List[(Int, Graph[VD, Double])]) : List[(Int, Graph[VD, Double])] = {
        if (i <= numberOfSignificantBits) {
            // select only edges whose binary representation of the weight has a '1' on the i-th index
            val newEdges = edgesWithBinaryMapping.filter(tuple => tuple._2(i) == 1).map(tuple => tuple._1)
            val newGraph = Graph(subGraph.vertices, newEdges)
            val newUnion = resultingGraphSet.union(List((i, newGraph))).distinct
            decompose(i + 1, newUnion)
        } else {
            resultingGraphSet
        }
    }
    
    // start off from index 1, because getBinaryFormOfFraction produces on index 0 the first non-fractional digit, i.e. 0, which should not be considered
    val decomposedGraphs = decompose(1, List[(Int, Graph[VD, Double])]())
    
    // list of (i, e^i), where e^i is the edgeset of the decomposed graph g^i
    val edgesOfDecomposedGraphs = decomposedGraphs.map(tuple => (tuple._1, tuple._2.edges))
    
    def createUnionizedEdgeSet(i : Int, resultingEdgeSet : List[(Int, RDD[Edge[Double]])]) : List[(Int, RDD[Edge[Double]])] = {
        if (i < edgesOfDecomposedGraphs.length) {
            val nextEdgeSet = edgesOfDecomposedGraphs(i)._2
            val previousEdgeSet = resultingEdgeSet(i - 1)._2
            val newEdgeSet = previousEdgeSet.union(nextEdgeSet)
            val newUnion = resultingEdgeSet.union(List((i, newEdgeSet)))
            createUnionizedEdgeSet(i + 1, newUnion)
        }
        else {
            resultingEdgeSet
        }
    }
    
    // list of (i, unionEdgeSet(i)), where each edge set is the union of it and its predecessors (i.e. {a,b,c} => {a, a u b, a u b u c,})
    val unionizedEdgeSets = createUnionizedEdgeSet(1, List((0, edgesOfDecomposedGraphs.head._2)))
    
    def extractConnectedComponents(i : Int, resultingComponentSet : List[(Int, RDD[(VertexId, List[VertexId])])]) : List[(Int, RDD[(VertexId, List[VertexId])])] = {
        if (i < unionizedEdgeSets.length) {
            // create a new graph from the vertices of the original graph and the current unionized edge set
            val newEdges = unionizedEdgeSets(i)._2
            val newGraph = Graph[VD, Double](subGraph.vertices, newEdges)
            
            // collection of tuples (x, y), where x is the VertexId and y is the connected components group it belongs to (more precisely, the lowest VertexId of all the vertices in that group)
            val connectedVertices = newGraph.connectedComponents().vertices
            
            // first swap the above tuples to (y, x), then aggregate by y to RDD[(VertexId, List[VertexId])] (e.g. (y, {x1, x2, ...}))
            // aggregate(emptyList)(seqOp, combOp), where seqOp adds the new x element to the existing accumulator list, and combOp concatenates two accumulator lists together
            val aggregatedVertices = connectedVertices.map(tuple => tuple.swap).aggregateByKey(List[VertexId]())((accumulator: List[VertexId], element: VertexId) => accumulator ++ List(element), (accumulator1: List[VertexId], accumulator2: List[VertexId]) => accumulator1 ++ accumulator2)
            
            val newUnion = resultingComponentSet.union(List((i, aggregatedVertices)))
            extractConnectedComponents(i + 1, newUnion)
        }
        else {
            resultingComponentSet
        }
    }
    
    // the connected components resulting from a graph created by using the subgraph vertices and the edges from unionized edge set; for each unionized edge set the graph is created and its connected components determined
    val extractedConnectedComponents = extractConnectedComponents(0, List[(Int, RDD[(VertexId, List[VertexId])])]())
    
    def getPullbackFromEdgeSet(edgeTuple : (Int, RDD[Edge[Double]])) : Graph[VD, Double] = {
        val edges = edgeTuple._2
        val i = edgeTuple._1
        
        // set of vertices attached to the provided edges
        val attachedVertices = edges.flatMap(edge => List(edge.dstId, edge.srcId)).distinct
        
        // a fullOuterJoin works only with PairRDDs, so mapping the attachedVertices with dummy values
        val attachedVerticesTuples = attachedVertices.map(vertex => (vertex, -1))
        
        // RDD[(VertexId, List[VertexId])], i.e. the collection of connected components of the i-th edge set (D{^i}{_1}, ..., {^i}{_n})
        val ithConnectedComponents = extractedConnectedComponents.filter(tuple => i == tuple._1).head._2
        
        // collect an array of VertexIds, that correspond to each of the connected component groups
        val connectedComponentIds = ithConnectedComponents.map(_._1).collect
        
        // returns tuples in form (u, pi(u)), where u is a vertex from the interesting components set and pi(u) is the map of the partition
        def computeUnionOfInterestingComponents(index : Int, resultingSet : RDD[(VertexId, Int)]) : RDD[(VertexId, Int)] = {
            if (index < connectedComponentIds.length) {
                // RDD[List[VertexId]], the vertices that constitute the index-th connected component
                val connectedVertices = ithConnectedComponents.filter(tuple => tuple._1 == connectedComponentIds(index)).flatMap(tuple => tuple._2)
                
                // create intersections between the connected component and the vertices of the given edge
                val intersectedVertices = attachedVertices.intersection(connectedVertices).distinct
                
                // a fullOuterJoin works only with PairRDDs, so mapping the intersectedVertices with dummy values
                val intersectedVerticesTuples = intersectedVertices.map(vertex => (vertex, index))
                
                // check if any of the edges is on the boundary of the generated set
                val intersectedVerticesIsEmpty = intersectedVertices.isEmpty
                val intersectionIsEmpty = intersectedVerticesTuples.fullOuterJoin(attachedVerticesTuples).filter(tuple => tuple._2._1 == None || tuple._2._2 == None).isEmpty

                if (!intersectedVerticesIsEmpty && !intersectionIsEmpty) {
                    val newUnion = intersectedVerticesTuples.union(resultingSet)
                    computeUnionOfInterestingComponents(index + 1, newUnion)
                } else {
                    computeUnionOfInterestingComponents(index + 1, resultingSet)
                }         
            } else {
                resultingSet
            }       
        }
        
        val interestingComponents = computeUnionOfInterestingComponents(0, graph.edges.context.emptyRDD[(VertexId, Int)])
        
        if (interestingComponents.count() > 0) {
            // the vertices of the contraction graph are the interesting component vertices
            val contractionVertices : RDD[(VertexId, Int)] = interestingComponents.map(tuple => (tuple._2, 1))
            
            // adds an index to each edge, i.e. RDD[(Edge[Double], Long)]
            val indexedEdges = edges.zipWithIndex()
            
            // maps the edge weights to the edge indices, RDD[(Long, Double)]
            val indexedEdgeWeights = indexedEdges.map(tuple => (tuple._2, tuple._1.attr))
            
            // splits the indexed edges to (dstId, index) tuples, i.e. RDD[(VertexId, Long)]
            val indexedDestinationVertices = indexedEdges.map(tuple => (tuple._1.dstId, tuple._2))
            
            // splits the indexed edges to (srcId, index) tuples, i.e. RDD[(VertexId, Long)]
            val indexedSourceVertices = indexedEdges.map(tuple => (tuple._1.srcId, tuple._2))
            
            // performs a join by the destination vertex id, resulting in a map vertexId => (interestingcomponents index, edges index), i.e. RDD[(VertexId, (Int, Long))], then rearranges the tuple to RDD[(Long, (VertexId, Int))], so that the edge id can be mapped with regard to its constitutent vertices
            val combinedDestinationIndices = interestingComponents.join(indexedDestinationVertices).map(tuple => (tuple._2._2, (tuple._1, tuple._2._1)))
            
            // performs a join by the source vertex id, resulting in a map vertexId => (interestingcomponents index, edges index), i.e. RDD[(VertexId, (Int, Long))], then rearranges the tuple to RDD[(Long, (VertexId, Int))], so that the edge id can be mapped with regard to its constitutent vertices
            val combinedSourceIndices = interestingComponents.join(indexedSourceVertices).map(tuple => (tuple._2._2, (tuple._1, tuple._2._1)))
            
            // performs a join by the edge index, resulting in a map edge index => (edges index, (u, pi(u)), (v, pi(v))), i.e. RDD[(Long, ((VertexId, Int), (VertexId, Int)))]
            val mappedSourceDestinationCombinations = combinedDestinationIndices.join(combinedSourceIndices)
            
            // cut off the excess information from mappedSourceDestinationCombinations to (edges index, (pi(u), pi(v))), i.e. RDD[(Long, (Int, Int))]
            val indexedPartitionMappedEdges = mappedSourceDestinationCombinations.map(tuple => (tuple._1, (tuple._2._2._1, tuple._2._2._2)))
            
            // combine (edges index, (pi(u), pi(v))) with (edges index, edge weight)), i.e. RDD[(Long, (Int, Int))] join RDD[(Long, Double)] => RDD[(Long, ((Int, Int), Double))], then filter out all self loops, i.e. where pi(u) == pi(v)
            val weightedMappedSourceDestinationCombinations = indexedPartitionMappedEdges.join(indexedEdgeWeights).filter(tuple => tuple._2._1._1 != tuple._2._1._2)    
            
            // omit the edge index, i.e. RDD[(Long, ((Int, Int), Double))] => RDD[((Int, Int), Double)], then aggregate all weights to a sum, associated with the same (pi(u), p(v)), and convert the set to an EdgeRDD[Double]
            val contractionEdges = weightedMappedSourceDestinationCombinations.map(tuple => (tuple._2._1, tuple._2._2)).aggregateByKey(0.0)((acc : Double, elem : Double) => acc + elem, (acc1 : Double, acc2 : Double) => acc1 + acc2).map(tuple => Edge(tuple._1._1, tuple._1._2, tuple._2))
        
            val contractionGraph = Graph(contractionVertices, contractionEdges)
            
            val sparsifiedContractionGraph = boundedSparsify(contractionGraph, epsilon / 6, p / (2 * subgraphNumberOfVertices * l))

            // convert mappedSourceDestinationCombinations (edges index, ((u, pi(u)), (v, pi(v)))) to ((pi(u), pi(v)), (u, v)). in order to avoid confusion with swapped pi(u) and pi(v) values, arrange pi(u) and pi(v) in ascending order, u and v follow the order of pi(u) and pi(v)
            val partitionMappedEdges = mappedSourceDestinationCombinations.map(tuple => if (tuple._2._1._2 < tuple._2._2._2) ((tuple._2._1._2, tuple._2._2._2), (tuple._2._1._1, tuple._2._2._1)) else ((tuple._2._2._2, tuple._2._1._2), (tuple._2._2._1, tuple._2._1._1)))

            // converts the sparsifiedContractionGraph edges into ((pi(u), pi(v)), weight), suitable for matching with partitionMappedEdges. in order to avoid confusion with swapped src and dst values, arrange srcId and dstId in ascending order
            val boundedSparsifyEdges = sparsifiedContractionGraph.edges.map(edge => if (edge.srcId < edge.dstId) ((edge.srcId.toInt, edge.dstId.toInt), edge.attr) else ((edge.dstId.toInt, edge.srcId.toInt), edge.attr))

            // join boundedSparsifyEdges and partitionMappedEdges by (pi(u), pi(v)), resulting in ((pi(u), pi(v)), (weight, (u, v)))
            val joinedPartitionMapAndEdgeData = boundedSparsifyEdges.join(partitionMappedEdges)

            // remove duplicate (pi(u), pi(v)) entries from joinedPartitionMapAndEdgeData and create pullback edges from the second tuple ((weight, (u, v)))
            val pullbackEdges = joinedPartitionMapAndEdgeData.reduceByKey((tuple1, tuple2) => tuple1).map(tuple => Edge(tuple._2._2._1, tuple._2._2._2, tuple._2._1))

            // return the pullback graph
            Graph(subGraph.vertices, pullbackEdges)
        }
        else {
            // return graph with no edges
            Graph(subGraph.vertices, graph.edges.context.emptyRDD[Edge[Double]])
        }
    }
    
    val pullbackGraphs = unionizedEdgeSets.map(tuple => getPullbackFromEdgeSet(tuple))
    
    pullbackGraphs.reduce((graph1, graph2) => sum(graph1, graph2))
  }
  
  /**
   * Converts a given floating point fraction into its binary representation.
   * 
   * @param fraction The floating point fraction to convert.
   * @param digits The number of digits required of the conversion.
   * @return The binary representation of the given floating point fraction as a list of binary numbers.
   */
  private def getBinaryFormOfFraction(fraction: Double, digits: Int) : List[Int] = List.tabulate(digits + 1)(i => ((fraction * math.pow(2, i)).toInt % 2))

  
  /**
   * Sparsifies the given graph in an unweighted manner, i.e. treats every edge weight as 1.
   * 
   * @param subGraph The graph for sparsification.
   * @param epsilon The epsilon parameter.
   * @param p The p parameter.
   * @return A sparsified graph.
   */
  private def boundedSparsify[VD:ClassTag, ED](subGraph : Graph[VD, Double], epsilon : Double, p : Double) : Graph[VD, Double] = {
    // get max weight from edge set to identify the number of "weight-1" graphs
    val maxWeight = subGraph.edges.max()(new Ordering[Edge[Double]]() {override def compare(x : Edge[Double], y: Edge[Double]) : Int = Ordering[Double].compare(x.attr, y.attr)}).attr
    val numberOfNewGraphs = math.ceil(math.abs(math.log(maxWeight) / math.log(2))).toInt
    
    val newP = p / numberOfNewGraphs

    // join each edge with the binary representation of its weight
    val edgesWithBinaryMapping = subGraph.edges.map(edge => (edge, getBinaryString(edge.attr.toInt, numberOfNewGraphs))).cache()

    def decompose(i : Int, resultingGraphSet : List[Graph[VD, Double]]) : List[Graph[VD, Double]] = {
        if (i < numberOfNewGraphs) {
            // select only edges whose binary representation of the weight has a '1' on the i-th index
            val newEdges = edgesWithBinaryMapping.filter(tuple => tuple._2(i) == '1').map(tuple => tuple._1)
            val newGraph = Graph(subGraph.vertices, newEdges)
            val sparsifiedGraph = unweightedSparsify(newGraph, epsilon, newP)
            val newUnion = resultingGraphSet.union(List(sparsifiedGraph))
            decompose(i + 1, newUnion)
        } else {
            resultingGraphSet
        }
    }
    
    val decomposedGraphs = decompose(0, List[Graph[VD, Double]]())
    decomposedGraphs.reduce((graph1, graph2) => sum(graph1, graph2))
  }
  
  /**
   * Converts a given integer into its binary representation.
   * 
   * @param number The number to convert.
   * @param digits The number of digits required of the conversion.
   * @return The binary representation of the given number as a string with leading zeroes up to digits positions.
   */
  private def getBinaryString(number: Int, digits: Int) : String = String.format("%" + digits + "s", number.toBinaryString).replace(' ', '0')
 
  
  /**
   * Sparsifies the given graph in an unweighted manner, i.e. treats every edge weight as 1.
   * 
   * @param subGraph The graph for sparsification.
   * @param epsilon The epsilon parameter.
   * @param p The p parameter.
   * @return A sparsified graph.
   */
  private def unweightedSparsify[VD:ClassTag, ED](subGraph : Graph[VD, Double], epsilon : Double, p : Double) : Graph[VD, Double] = {
    val subgraphVertexSet = subGraph.vertices.map(_._1)
    val subgraphVolume = volume(subgraphVertexSet)
    val subgraphNumberOfVertices = subGraph.vertices.count()
    
    val c3 = 1800
    val exitCriterion = (c3 * subgraphNumberOfVertices * math.pow(math.log10(subgraphNumberOfVertices / p) / math.log10(2), 30)) / (epsilon * epsilon)
    
    if (subgraphVolume <= exitCriterion) {
        subGraph
    }
    else {
        val phi = 1 / (2 * math.log10(subgraphVolume) / math.log10(29.0/28))
        val pCapped = (6 * subgraphNumberOfVertices * math.log10(subgraphNumberOfVertices) / math.log10(2.0))
        val epsilonCapped = (epsilon * math.log(2) * math.log(2)) / ((1 + 2 * math.log10(subgraphNumberOfVertices) / math.log10(29/28)) * (2 * math.log10(subgraphNumberOfVertices) / math.log10(2)))
    
        val partitionedGraphs = partitionAndSample(subGraph, phi, epsilonCapped, pCapped)
        
        val borderEdges = getBorderEdges(subGraph.edges, partitionedGraphs)
        
        val seedGraph = Graph(subGraph.vertices, borderEdges)
        
        val newGraph = unweightedSparsify(seedGraph, epsilon, p)
        
        partitionedGraphs.union(List(newGraph)).reduce((graph1, graph2) => sum(graph1, graph2))
    }
  }
  
  /**
   * Identifies the border edges, i.e. the edges that are between different vertex sets.
   * 
   * @param subGraphEdges The edge set of the original subgraph.
   * @param partitionedGraphs A collection of graphs, which are the result of partitioning the original subgraph.
   * @return A collection of edges that are not part of any of the provided partitioned graphs.
   */
  private def getBorderEdges[VD:ClassTag, ED](subGraphEdges : RDD[Edge[Double]], partitionedGraphs : List[Graph[VD, Double]]) : RDD[Edge[Double]] = {
    val collectionOfEdgeSets : List[RDD[Edge[Double]]] = partitionedGraphs.map(graph => graph.edges)
    val edgesFromAllPartitions = collectionOfEdgeSets.reduce((edgeSet1, edgeSet2) => edgeSet1.union(edgeSet2))
    subGraphEdges.subtract(edgesFromAllPartitions)
  }
  
  /**
   * Sums up two graphs, by producing a graph whose Laplacian matrix is equal to the sum of the Lapalacian matrices of the graphs.
   * In practical terms, this means that every edge in the resulting graph is equal to the sum of the corresponding edges in the two constituent graphs. In the case that the vertex sets are disjoint, the sum is a simple union of graphs.
   * 
   * @param first The first graph.
   * @param second The second graph.
   * @return The sum of the two graphs.
   */
  def sum[VD:ClassTag, ED](first: Graph[VD, Double], second : Graph[VD, Double]) : Graph[VD, Double] = {
    // vertices are a simple union of distinct values
    val newVertices = first.vertices.union(second.vertices).distinct
    
    // a uniform designation of the edge's src and dst vertices is needed so identical edges can be found. here, this is done by assuring an ascending order of vertex ids, i.e. (b, a) => (a, b) if a < b
    val firstEdges = first.edges.map(e => ( if (e.srcId < e.dstId) (e.srcId, e.dstId) else (e.dstId, e.srcId), e))
    val secondEdges = second.edges.map(e => ( if (e.srcId < e.dstId) (e.srcId, e.dstId) else (e.dstId, e.srcId), e))
    
    // a full outer join produces an RDD of tuples ((VertexId, VertexId), (Option[Edge[Double]], Option[Edge[Double]])), that matches edges from both sets according to their coordinates (which were normalized in the previous step), the optional edge parameters are set to none if the respective graph doesn't contain the particular edge, otherwise it's Option[edge]
    val joinedSet = firstEdges.fullOuterJoin(secondEdges)
    
    def extractEdgeWeight(edge : Option[Edge[Double]]) : Double = edge match { 
        case Some(x) => x.attr
        case None => 0.0
    }
        
    // converts a tuple from the full join to an Edge[Double] object, the vertex ids are kept, but the weight is the sum of the corresponding edge weights in the graphs (0 if the edge doesn't exist in the graph)
    def convertTupleToEdge(tuple : ((VertexId, VertexId), (Option[Edge[Double]], Option[Edge[Double]]))) : Edge[Double] = {
        val firstWeight = extractEdgeWeight(tuple._2._1)
        val secondWeight = extractEdgeWeight(tuple._2._2)
        
        Edge(tuple._1._1, tuple._1._2, firstWeight + secondWeight)
    }
    
    val newEdges = joinedSet.map(tuple => convertTupleToEdge(tuple))
    Graph[VD, Double](newVertices, newEdges)
  }
  
  /**
   * Partitions the graph by calling approxCut.
   * 
   * @param phi The p parameter.
   * @param epsilon The epsilon parameter.
   * @param p The p parameter.
   * @return The result of running sample on the graphs induced on the vertex sets of a decomposition of the original graph.
   */
  private def partitionAndSample[VD:ClassTag, ED](subGraph : Graph[VD, ED], phi : Double, epsilon : Double, p : Double) : List[Graph[VD, Double]] = {
    def getLambda(numberOfEdges : Long) : Double = {
        val logOfNumberOfEdges = math.log10(numberOfEdges) / math.log10(2)
        val c2 = 280
        val f2 = c2 * phi * phi / (logOfNumberOfEdges * logOfNumberOfEdges * logOfNumberOfEdges * logOfNumberOfEdges)
        0.5 * f2 * f2
    }
    
    def innerLoop[VD:ClassTag, ED](subGraph : Graph[VD, ED], phi : Double, epsilon : Double, p : Double, resultingGraphSet : List[Graph[VD, Double]]) : List[Graph[VD, Double]] = {
        val lambda = getLambda(subGraph.edges.count())
        
        val approxCutResult = approxCut(subGraph, phi, p)
        
        val subgraphVertexSet = subGraph.vertices.map(_._1)
        
        // exite criterion, there is nothing left to cut
        if (approxCutResult.count() == 0) {
            val sampledGraph = sample(subGraph, epsilon, p, lambda)
            resultingGraphSet.union(List(sampledGraph))
        } 
        // case when approxCut returns a small cut. this means that the complement is contained in a subgraph of large conductance.
        else if (volume(approxCutResult) <= volume(subgraphVertexSet) / 29) {
            val approxCutSplitGraph = createVertexInducedSubgraph(subGraph, approxCutResult)            
            val approxCutComplementGraph = createVertexInducedSubgraph(subGraph, subgraphVertexSet.subtract(approxCutResult))
            
            val sampledGraph = sample(approxCutComplementGraph, epsilon, p, lambda)
            val newUnion = resultingGraphSet.union(List(sampledGraph))
            innerLoop(approxCutSplitGraph, phi, epsilon, p, newUnion)
        }
        // case when approxCut returns a large cut. then act recursively on the cut and its complement in order to eventually partition and sample both
        else {
            val approxCutSplitGraph = createVertexInducedSubgraph(subGraph, approxCutResult)            
            val approxCutComplementGraph = createVertexInducedSubgraph(subGraph, subgraphVertexSet.subtract(approxCutResult))
            
            val firstResult = innerLoop(approxCutComplementGraph, phi, epsilon, p, resultingGraphSet)
            val secondResult = innerLoop(approxCutSplitGraph, phi, epsilon, p, resultingGraphSet)
            
            firstResult.union(secondResult)
        }
    }
    
    innerLoop(subGraph, phi, epsilon, p, List[Graph[VD, Double]]())
  }
  
  /**
   * Random sampling procedure for spectral sparsification of graphs.
   * 
   * @param subGraph The subgraph.
   * @param epsilon The epsilon parameter.
   * @param p The p parameter.
   * @param lambda Smallest non-zero normalized Laplacian eigenvalue.
   * @return The sparsified graph, with the identical vertex set, but scaled and reduced edge set. Formally, it is a (1 + epsilon)-approximation of the input graph.
   */
  private def sample[VD:ClassTag, ED](subGraph : Graph[VD, ED], epsilon : Double, p : Double, lambda : Double) : Graph[VD, Double] = {
    val k = math.max(math.log10(3 / p) / math.log10(2), math.log10(numberOfVertices) / math.log10(2))
    val upsilon = (144 * k * k) / (epsilon * epsilon * lambda * lambda)
    
    val vertexDegreeMap = vertexDegrees.collect().toMap
    
    // produce a RDD[(Edge, boolean)], where the edges are created to have the weight equal to the probability p_{i,j} and the boolean is set to true, if a random number (interval (0,1)) is less than p_{i,j}; and to false otherwise
    val edgeProbabilities = subGraph.edges.map(edge => { val probability = math.min(1.0, upsilon / math.min(vertexDegreeMap(edge.srcId), vertexDegreeMap(edge.dstId))); if (math.random < probability) (Edge(edge.srcId, edge.dstId, probability), true) else (Edge(edge.srcId, edge.dstId, probability), false)})
    
    // from RDD[(Edge, boolean)] filter out only those tuples that have the boolean set to true, i.e. edges that make are to be included in the sampled graph; then take only the first element of the tuple, i.e. the Edge[Double]
    val newEdges = edgeProbabilities.filter(_._2).map(_._1)
    
    // return the sampled graph with the unchanged vertex set and modified edge set
    Graph[VD, Double](subGraph.vertices, newEdges)
  }
  
  /**
   * Isolates a partition of the graph with the specified target conductance.
   * 
   * @param subGraph The set of vertices.
   * @param phi The target conductance.
   * @param p Parameter for partitioning.
   * @return A set of vertices with conductance less than phi.
   */
  private def approxCut[VD, ED](subGraph : Graph[VD, ED], phi : Double, p : Double) : RDD[VertexId] = {
    val r = math.ceil(math.log10(numberOfEdges) / math.log10(2))
    val epsilon = math.min(1 / (2 * r), 0.2)
    val vertices = subGraph.vertices.map(v => v._1)
    val lowerVolumeLimit = 0.8 * (if (vertices.count() == numberOfVertices) 2 * numberOfEdges else volume(vertices))
    val scaledPhi = 2 / 23 * phi
    val scaledP = p / (2 * r)
    
    def mainLoop(j : Int, vertexSet : RDD[VertexId], resultingUnionOfVertices : RDD[VertexId]) : RDD[VertexId] = {
        if (j < r && volume(vertexSet) >= lowerVolumeLimit) {
            // create a subgraph induced by the specified vertices; in the first call, that will be the whole subgraph
            val newSubGraph = createVertexInducedSubgraph(subGraph, vertexSet)
            
            val resultingSetOfVertices = partition2(newSubGraph, scaledPhi, scaledP, epsilon)
            
            // the set of vertices out of which the next subgraph will be created is the difference between the current set and the result of partition
            val newSetOfVertices = vertexSet.subtract(resultingSetOfVertices)
            
            // the union that is gradually built from results of partition and that is ultimately returned as result, once the recursion finishes
            val newUnion = resultingUnionOfVertices.union(resultingSetOfVertices).distinct()
            
            // recursive call, increment the counter, pass the reduced set of vertices, and continue expanding the union
            mainLoop(j + 1, newSetOfVertices, newUnion)
        }
        else {
            resultingUnionOfVertices
        }
    }
    
    // start off with j = 0, all vertices in the subgraph and an empty result set
    mainLoop(0, vertices, graph.edges.context.emptyRDD[VertexId])
  }
  
  private def partition2[VD, ED](subGraph : Graph[VD, ED], theta : Double, p : Double, epsilon : Double) : RDD[VertexId] = {
    val r = math.ceil(math.log10(1 / epsilon) / math.log10(2))
    val vertices = subGraph.vertices.map(_._1)
    val lowerVolumeLimit = 0.8 * (if (vertices.count() == numberOfVertices) 2 * numberOfEdges else volume(vertices))
    
    def mainLoop(j : Int, vertexSet : RDD[VertexId], resultingUnionOfVertices : RDD[VertexId]) : RDD[VertexId] = {
        if (j < r && volume(vertexSet) >= lowerVolumeLimit) {
            // create a subgraph induced by the specified vertices; in the first call, that will be the whole subgraph
            val newSubGraph = createVertexInducedSubgraph(subGraph, vertexSet)
            
            val resultingSetOfVertices = partition(newSubGraph, theta / 9, p / r)
            
            // the set of vertices out of which the next subgraph will be created is the difference between the current set and the result of partition
            val newSetOfVertices = vertexSet.subtract(resultingSetOfVertices)
            
            // the union that is gradually built from results of partition and that is ultimately returned as result, once the recursion finishes
            val newUnion = resultingUnionOfVertices.union(resultingSetOfVertices).distinct()
            
            // recursive call, increment the counter, pass the reduced set of vertices, and continue expanding the union
            mainLoop(j + 1, newSetOfVertices, newUnion)
        }
        else {
            resultingUnionOfVertices
        }
    }
    
    // start off with j = 0, all vertices in the subgraph and an empty result set
    mainLoop(0, vertices, graph.edges.context.emptyRDD[VertexId])
  }
  
  private def partition[VD, ED](subGraph : Graph[VD, ED], theta : Double, p : Double) : RDD[VertexId] = {
    val phi = theta / 7
    
    // RDD[VertexId]
    val vertices = subGraph.vertices.map(_._1)
    val lowerVolumeLimit = 0.75 * (if (vertices.count() == numberOfVertices) 2 * numberOfEdges else volume(vertices))
    val upperCounterLimit = 12 * subGraph.edges.count().toInt * math.ceil(math.log10(1 / p))
    
    def mainLoop(j : Int, vertexSet : RDD[VertexId], resultingUnionOfVertices : RDD[VertexId]) : RDD[VertexId] = {
        if (j < upperCounterLimit && volume(vertexSet) >= lowerVolumeLimit) {
            // create a subgraph induced by the specified vertices; in the first call, that will be the whole subgraph
            val newSubGraph = createVertexInducedSubgraph(subGraph, vertexSet)
            
            val resultingSetOfVertices = randomNibble(newSubGraph, phi)
            
            // the set of vertices out of which the next subgraph will be created is the difference between the current set and the result of randomNibble
            val newSetOfVertices = vertexSet.subtract(resultingSetOfVertices)
            
            // the union that is gradually built from results of randomNibble and that is ultimately returned as result, once the recursion finishes
            val newUnion = resultingUnionOfVertices.union(resultingSetOfVertices).distinct()
            
            // recursive call, increment the counter, pass the reduced set of vertices, and continue expanding the union
            mainLoop(j + 1, newSetOfVertices, newUnion)
        }
        else {
            resultingUnionOfVertices
        }
    }
    
    // start off with j = 0, all vertices in the subgraph and an empty result set
    mainLoop(0, vertices, graph.edges.context.emptyRDD[VertexId])
  }
  
  private def createVertexInducedSubgraph[VD, ED](subGraph : Graph[VD, ED], vertexSet : RDD[VertexId]) : Graph[VD, ED] = {
    if (subGraph.vertices.count() == vertexSet.count()) {
        subGraph
    }
    else {
        val vertexIdHash = HashSet() ++ vertexSet.collect()
        subGraph.subgraph(edge => true, (vid, vd) => vertexIdHash.contains(vid))
    }
  }
  
  private def randomNibble[VD, ED](subGraph : Graph[VD, ED], phi : Double) : RDD[VertexId] = {  
    val numberOfVertices = subGraph.vertices.count().toInt
    val numberOfEdges = subGraph.edges.count()
    
    val volumeOfVertexSet : Double = 2 * numberOfEdges    
    val vertex = vertexDegrees().join(subGraph.vertices).map(tuple => (tuple._1, tuple._2._1 / volumeOfVertexSet)).reduce((tuple1, tuple2) => if (tuple1._2 >= tuple2._2) tuple1 else tuple2)._1
    
    val upperValueOfB = math.ceil(math.log10(numberOfEdges)).toInt
    val b = (1 to upperValueOfB).map(i => (i, math.pow(2, -i) / (1 - math.pow(2, -upperValueOfB)))).reduce((tuple1, tuple2) => if (tuple1._2 >= tuple2._2) tuple1 else tuple2)._1
    
    nibble(subGraph, vertex, phi, b)
  }
  
  private def nibble[VD, ED](subGraph : Graph[VD, ED], vertex : VertexId, phi : Double, b : Int) : RDD[VertexId] = {
    val numberOfVertices = subGraph.vertices.count().toInt
    val numberOfEdges = subGraph.edges.count().toInt
    
    val c1 = 200
    val c3 = 1800
    val c4 = 140
    val l = math.ceil(math.log10(numberOfEdges) / math.log10(2.0)).toInt
    val t1 = math.ceil(2 / (phi * phi) * math.log(c1 * (l + 2) * math.sqrt(numberOfEdges))).toInt
    val t_last = t1 * (l + 1)
    
    val epsilon = 1 / (c3 * (l + 2) * t_last * math.pow(2, b))
    
    val c5 = (l + 2) * math.pow(2, b) / c4
    
    // fill in the degrees of the vertices in the subset in ascending order
    val degrees = Array.fill(numberOfVertices)(0.0)
    
    // map VertexId -> Index
    val vertexIdToIndexMap = subGraph.vertices.map(_._1).sortBy(i => i, true).zipWithIndex().collect().toMap //map(tuple => tuple._1 -> tuple._2).collect.toMap

    // update the degrees array with the degrees of the original graph, not the subgraph
    vertexDegrees().sortBy(tuple => tuple._1, true).collect().foreach(tuple => if (vertexIdToIndexMap.contains(tuple._1)) degrees(vertexIdToIndexMap(tuple._1).toInt) = tuple._2) 
    
    var q = Array.fill(numberOfVertices)(0.0)
    q(vertexIdToIndexMap(vertex).toInt) = 1.0
    
    var r = Array.tabulate(q.length){ i => if (q(i) >= degrees(i) * epsilon) q(i) else 0.0}
    
    // calculate M = (AD^-1 + I) / 2, which for each entry boils down to: M(i, j) = A(i, j) / (2 * D(i)) and M(i, i) = (A(i, i) + 1) / (2 * D(i))
    val randomWalkEntries = adjacencyMatrixOfSubGraph(subGraph, vertexIdToIndexMap).entries.map(entry => if (entry.i == entry.j) MatrixEntry(entry.i, entry.j, (entry.value + 1) / (2 * degrees(entry.i.toInt))) else MatrixEntry(entry.i, entry.j, entry.value / (2 * degrees(entry.i.toInt))))
    val randomWalkCoordinateMatrix = new CoordinateMatrix(randomWalkEntries, numberOfVertices, numberOfVertices)
    val randomWalkMatrix = randomWalkCoordinateMatrix.toIndexedRowMatrix()//.cache()
    
    def findSuitableSet(sortedSet : RDD[((VertexId, Double),Long)], j : Int) : RDD[VertexId] = {
        // extract an RDD[VertexId] from the sortedSet by selecting all tuples whose index is less then j and then keeping only the VertexId part
        val currentSet = sortedSet.filter(tuple => tuple._2.toInt < j).map(tuple => tuple._1._1)
        val currentSetVolume = volume(currentSet)
        val currentSetConductance = conductance(subGraph, currentSet)
        
        if (currentSetConductance <= phi && currentSetVolume <= 5 * numberOfEdges / 3 && currentSetVolume >= math.pow(2, b) && q(j) / degrees(j) >= c5) {
            currentSet
        }
        else if (j == q.length - 1) {
            graph.edges.context.emptyRDD[VertexId]
        }
        else {
            findSuitableSet(sortedSet, j + 1)
        }
    }
    
    def mainLoop(t : Int) : RDD[VertexId] = {
        val rToMatrix = Matrices.dense(r.length, 1, r)
            
        // generate q_t = M * r_{t-1}
        randomWalkMatrix.multiply(rToMatrix).rows.collect().foreach(row => q(row.index.toInt) = row.vector(0))
        
        // generate r_t for the next iteration
        r = Array.tabulate(q.length){ i => if (q(i) >= degrees(i) * epsilon) q(i) else 0.0}
        
        // generate a set S_j(q), sorted in descending order of values q(i) / d(i) - this is an RDD[((VertexId, quotient),Index)], sorted in descending order by quotient
        val sortedSet = subGraph.vertices.map(tuple => (tuple._1, q(vertexIdToIndexMap(tuple._1).toInt) / degrees(vertexIdToIndexMap(tuple._1).toInt))).sortBy(tuple => tuple._2, false).zipWithIndex().cache()
        
        // attempt to find a suitable set for this iteration
        val suitableSet = findSuitableSet(sortedSet, 1)
        
        // return the suitable set, if it has any elements; return an empty set, if no elements were found and the iteration counter reached its limit; recursively call the mainLoop otherwise
        if (suitableSet.count() > 0 || t == t_last) {
            suitableSet
        }
        else {
            mainLoop(t + 1)
        }
    }
    
    mainLoop(1)
  }
  
  /**
   * Calculates the adjacency matrix of the provided graph.
   * Each element at coordinates (i,j) of the adjacency matrix is set to 1, it there is an edge that connects the vertices i and j in the graph, and 0 otherwise.
   * 
   * @param subGraph The subgraph.
   * @param vertexIdToIndexMap Maps the vertexId to its index.
   * @returns A [[CoordinateMatrix]] representing the adjacency matrix.
   */
  private def adjacencyMatrixOfSubGraph[VD, ED](subGraph : Graph[VD, ED], vertexIdToIndexMap : Map[VertexId, Long]): CoordinateMatrix = { 
    val numberOfVertices = subGraph.vertices.count().toInt
  
    // A(u,v) = 1, if (u,v) e E and u =/= v
    val matrixentries1 = subGraph.edges.map(edge => MatrixEntry(vertexIdToIndexMap(edge.srcId), vertexIdToIndexMap(edge.dstId), 1.0))
    val matrixentries2 = subGraph.edges.map(edge => MatrixEntry(vertexIdToIndexMap(edge.dstId), vertexIdToIndexMap(edge.srcId), 1.0))
    
    // A(u,v) = k, if u = v and this vertex has k self-loops
    // usually, the adjacency matrix has 0 on its main diagonal. here, they will be substituted by the difference of the vertex' original degree and subgraph degree
    val subgraphDegrees = vertexDegreesOfSubGraph(subGraph).sortBy(tuple => tuple._1, true)
    val graphDegrees = vertexDegrees().filter(tuple => vertexIdToIndexMap.contains(tuple._1)).sortBy(tuple => tuple._1, true)
    val combinedGraphSubgraph = graphDegrees.zip(subgraphDegrees)
    val diagonalEntries = combinedGraphSubgraph.map(tuple => MatrixEntry(vertexIdToIndexMap(tuple._1._1), vertexIdToIndexMap(tuple._1._1), tuple._1._2 - tuple._2._2))

    new CoordinateMatrix(matrixentries1 ++ matrixentries2 ++ diagonalEntries, numberOfVertices, numberOfVertices)
  }
  
  /**
   * Calculates the degree of each vertex in the graph.
   * 
   * @param subGraph The subgraph.
   * @return An RDD that maps each vertex id to its degree.
   */
  private def vertexDegreesOfSubGraph[VD, ED](subGraph : Graph[VD, ED]) : RDD[(VertexId, Int)] = subGraph.aggregateMessages[Int](ctx => { ctx.sendToSrc(1); ctx.sendToDst(1) }, _+_).cache()
  
  /**
   * Calculates the conductance of the provided set of vertices with reference to the given subgraph.
   * 
   * @param subGraph The subgraph.
   * @param vertexSet The set of vertices.
   * @return The conductance of the provided set of vertices.
   */
  def conductance[VD, ED](subGraph : Graph[VD, ED], vertexSet : RDD[VertexId]) : Double = {
    val vertices = vertexSet.collect()
    val numberOfCrossSetEdges = subGraph.edges.filter(edge => (vertices.contains(edge.srcId) && !vertices.contains(edge.dstId)) || (vertices.contains(edge.dstId) && !vertices.contains(edge.srcId))).count()
    
    // However, when measuring the conductance and volumes of vertices in these vertex-induced subgraphs, we will continue to measure the volume according to the degrees of vertices in the original graph.
    val setVolume = volume(vertexSet).toDouble
    val remainingVolume = 2 * numberOfEdges - setVolume
    
    if (setVolume < remainingVolume)
    {
        numberOfCrossSetEdges / setVolume
    }
    else
    {
        numberOfCrossSetEdges / remainingVolume
    }
  }
  
  /**
   * Calculates the volume of a set of vertices, i.e. the sum of the degrees of each vertex in the set.
   * 
   * @param vertexSet The set of vertices.
   * @return The sum of the degrees of each vertex in the set.
   */
  def volume(vertexSet : RDD[VertexId]) : Int = {
    if (numberOfVertices == vertexSet.count())
    {
        2 * numberOfEdges
    }
    else
    {
        // prepare for using join, join needs both RDDs to hold pairs, so expand the vertexSet with a dummy value
        val dummyVertexSet = vertexSet.map(vid => (vid, -1))
        val degrees = vertexDegrees()
        
        // join produces tuple (vertexId, (vertexDegree, -1))
        degrees.join(dummyVertexSet).reduce((tuple1, tuple2) => (tuple1._1, (tuple1._2._1 + tuple2._2._1, tuple1._2._2)))._2._1
    }
  }
  
  /**
   * Calculates the degree of each vertex in the graph.
   * 
   * @return An RDD that maps each vertex id to its degree.
   */
  private def vertexDegrees() : RDD[(VertexId, Int)] = graph.aggregateMessages[Int](ctx => { ctx.sendToSrc(1); ctx.sendToDst(1) }, _+_).cache()
}