package org.SpectralGraphAnalysis

import org.apache.spark
import org.apache.spark.SparkContext 
import org.apache.spark.graphx.Graph._
import org.apache.spark.graphx.PartitionStrategy._
import org.apache.spark.rdd._
import org.apache.spark.storage.StorageLevel
import org.apache.spark.util.Utils 
import org.scalatest.{BeforeAndAfterAll, FunSuite, Outcome}

class SpectralGraphTests extends FunSuite with BeforeAndAfterAll {
  var sc : SparkContext
  
  before {
    val conf = new SparkConf().setAppName("maintest").setMaster("local[*]")
    sc = new SparkContext(conf)
  }
  
  after {
    sc.stop()
  }
  
  test("adjacancyMatrix_ringGraph_correctMatrixReturned"){
    val edges: RDD[Edge[String]] = sc.parallelize(Array(Edge(0L, 1L, "v"), Edge(0L, 2L, "v"), Edge(0L, 3L, "v"), Edge(1L, 3L, "v"), Edge(3L, 4L, "v")))
    val graph = Graph.fromEdgeTuples(edges)
    
    val sut = new SpectralGraph(graph)
    val actualMatrixEntriesSet = sut.adjacancyMatrix().entries.collect().toSet
    val expectedMatrixEntriesSet = Set (MatrixEntry(0, 1, 1.0), MatrixEntry(1, 0, 1.0), MatrixEntry(0, 2, 1.0), MatrixEntry(2, 0, 1.0), MatrixEntry(0, 3, 1.0), MatrixEntry(3, 0, 1.0), MatrixEntry(1, 3, 1.0), MatrixEntry(3, 1, 1.0), MatrixEntry(3, 4, 1.0), MatrixEntry(4, 3, 1.0) )
    
    assert(expectedMatrixEntriesSet === actualMatrixEntriesSet)
  }
  
  test("laplacianMatrix_ringGraph_correctMatrixReturned"){
    val edges: RDD[Edge[String]] = sc.parallelize(Array(Edge(0L, 1L, "v"), Edge(0L, 2L, "v"), Edge(0L, 3L, "v"), Edge(1L, 3L, "v"), Edge(3L, 4L, "v")))
    val graph = Graph.fromEdgeTuples(edges)
    
    val sut = new SpectralGraph(graph)
    val actualMatrixEntriesSet = sut.laplacianMatrix().entries.collect().toSet
    val expectedMatrixEntriesSet = Set (MatrixEntry(0, 1, -1.0), MatrixEntry(1, 0, -1.0), MatrixEntry(0, 2, -1.0), MatrixEntry(2, 0, -1.0), MatrixEntry(0, 3, -1.0), MatrixEntry(3, 0, -1.0), MatrixEntry(1, 3, -1.0), MatrixEntry(3, 1, -1.0), MatrixEntry(3, 4, -1.0), MatrixEntry(4, 3, -1.0), MatrixEntry(0, 0, 3.0), MatrixEntry(1, 1, 2.0), MatrixEntry(2, 2, 1.0), MatrixEntry(3, 3, 2.0), MatrixEntry(4, 4, 1.0) )
    
    assert(expectedMatrixEntriesSet === actualMatrixEntriesSet)
  }
  
  test("laplacianRayleighQuotient_properlySizedVector_correctQuotientReturned"){
    val edges: RDD[Edge[String]] = sc.parallelize(Array(Edge(0L, 1L, "v"), Edge(0L, 2L, "v"), Edge(0L, 3L, "v"), Edge(1L, 3L, "v"), Edge(3L, 4L, "v")))
    val graph = Graph.fromEdgeTuples(edges)
    val vectorProbe = Array(1.0, 2.5, 3.0, 0.5, -1.0)
    
    val sut = new SpectralGraph(graph)
    val actualQuotient = sut.laplacianRayleighQuotient(vectorProbe)
    val expectedQuotient = 0.6142857
    
    assert(expectedQuotient === actualQuotient +- 0.0000001)
  }
  
  test("laplacianRayleighQuotient_mismatchedVector_operationFails"){
    val edges: RDD[Edge[String]] = sc.parallelize(Array(Edge(0L, 1L, "v"), Edge(0L, 2L, "v"), Edge(0L, 3L, "v"), Edge(1L, 3L, "v"), Edge(3L, 4L, "v")))
    val graph = Graph.fromEdgeTuples(edges)
    val vectorProbe = Array(1.0, 2.5, 3.0, 0.5)
    
    val sut = new SpectralGraph(graph)
    val actualQuotient = sut.laplacianRayleighQuotient(vectorProbe)  
  }
  
  test("laplacianQuadraticForm_properlySizedVector_correctResultReturned"){
    val edges: RDD[Edge[String]] = sc.parallelize(Array(Edge(0L, 1L, "v"), Edge(0L, 2L, "v"), Edge(0L, 3L, "v"), Edge(1L, 3L, "v"), Edge(3L, 4L, "v")))
    val graph = Graph.fromEdgeTuples(edges)
    val vectorProbe = Array(1.0, 2.5, 3.0, 0.5, -1.0)
    
    val sut = new SpectralGraph(graph)
    val actualQuotient = sut.laplacianQuadraticForm(vectorProbe)
    val expectedQuotient = 10.75
    
    assert(expectedQuotient === actualQuotient +- 0.01)
  }
  
  test("laplacianQuadraticForm_mismatchedVector_operationFails"){
    val edges: RDD[Edge[String]] = sc.parallelize(Array(Edge(0L, 1L, "v"), Edge(0L, 2L, "v"), Edge(0L, 3L, "v"), Edge(1L, 3L, "v"), Edge(3L, 4L, "v")))
    val graph = Graph.fromEdgeTuples(edges)
    val vectorProbe = Array(1.0, 2.5, 3.0, 0.5)
    
    val sut = new SpectralGraph(graph)
    val actualQuotient = sut.laplacianQuadraticForm(vectorProbe) 
  }
  
  test("tryCompare_graphsWithDifferentVertexCounts_operationFails"){
    val edges1: RDD[Edge[String]] = sc.parallelize(Array(Edge(0L, 1L, "v"), Edge(0L, 2L, "v"), Edge(0L, 3L, "v"), Edge(1L, 3L, "v"), Edge(3L, 4L, "v")))
    val graph1 = Graph.fromEdgeTuples(edges1)
    
    val edges2: RDD[Edge[String]] = sc.parallelize(Array(Edge(0L, 1L, "v"), Edge(0L, 2L, "v"), Edge(0L, 3L, "v"), Edge(0L, 4L, "v"), Edge(2L, 5L, "v")))
    val graph2 = Graph.fromEdgeTuples(edges2)
    
    val sut = new SpectralGraph(graph1)
    val result = sut.tryCompare(graph1, graph2)
  }
  
  test("tryCompare_firstGraphPrecedesSecond_returnsPositive"){
    val edges1: RDD[Edge[String]] = sc.parallelize(Array(Edge(0L, 1L, "v"), Edge(0L, 2L, "v"), Edge(0L, 3L, "v"), Edge(1L, 3L, "v"), Edge(3L, 4L, "v")))
    val graph1 = Graph.fromEdgeTuples(edges1)
    
    val edges2: RDD[Edge[String]] = sc.parallelize(Array(Edge(0L, 1L, "v"), Edge(0L, 2L, "v"), Edge(0L, 3L, "v"), Edge(0L, 4L, "v"), Edge(2L, 4L, "v")))
    val graph2 = Graph.fromEdgeTuples(edges2)
    
    val sut = new SpectralGraph(graph1)
    val result = sut.tryCompare(graph1, graph2)
    
    assert(result.get > 0)
  }
  
  test("tryCompare_firstGraphSuccedesSecond_returnsNegative"){
    val edges1: RDD[Edge[String]] = sc.parallelize(Array(Edge(0L, 1L, "v"), Edge(0L, 2L, "v"), Edge(0L, 3L, "v"), Edge(1L, 3L, "v"), Edge(3L, 4L, "v")))
    val graph1 = Graph.fromEdgeTuples(edges1)
    
    val edges2: RDD[Edge[String]] = sc.parallelize(Array(Edge(0L, 1L, "v"), Edge(0L, 2L, "v"), Edge(0L, 3L, "v"), Edge(0L, 4L, "v"), Edge(2L, 4L, "v")))
    val graph2 = Graph.fromEdgeTuples(edges2)
    
    val sut = new SpectralGraph(graph1)
    val result = sut.tryCompare(graph1, graph2)
    
    assert(result.get < 0)
  }
  
  test("tryCompare_equivalentGraphs_returnsZero"){
    val edges: RDD[Edge[String]] = sc.parallelize(Array(Edge(0L, 1L, "v"), Edge(0L, 2L, "v"), Edge(0L, 3L, "v"), Edge(1L, 3L, "v"), Edge(3L, 4L, "v")))
    val graph = Graph.fromEdgeTuples(edges)
    
    val sut = new SpectralGraph(graph)
    val result = sut.tryCompare(graph, graph)
    
    assert(result.get === 0)
  }
  
  test("lteq_graphsWithDifferentVertexCounts_operationFails"){
    val edges1: RDD[Edge[String]] = sc.parallelize(Array(Edge(0L, 1L, "v"), Edge(0L, 2L, "v"), Edge(0L, 3L, "v"), Edge(1L, 3L, "v"), Edge(3L, 4L, "v")))
    val graph1 = Graph.fromEdgeTuples(edges1)
    
    val edges2: RDD[Edge[String]] = sc.parallelize(Array(Edge(0L, 1L, "v"), Edge(0L, 2L, "v"), Edge(0L, 3L, "v"), Edge(0L, 4L, "v"), Edge(2L, 5L, "v")))
    val graph2 = Graph.fromEdgeTuples(edges2)
    
    val sut = new SpectralGraph(graph1)
    val result = sut.lteq(graph1, graph2)
  }
  
  test("lteq_firstGraphPrecedesSecond_returnsTrue"){
    val edges1: RDD[Edge[String]] = sc.parallelize(Array(Edge(0L, 1L, "v"), Edge(0L, 2L, "v"), Edge(0L, 3L, "v"), Edge(1L, 3L, "v"), Edge(3L, 4L, "v")))
    val graph1 = Graph.fromEdgeTuples(edges1)
    
    val edges2: RDD[Edge[String]] = sc.parallelize(Array(Edge(0L, 1L, "v"), Edge(0L, 2L, "v"), Edge(0L, 3L, "v"), Edge(0L, 4L, "v"), Edge(2L, 4L, "v")))
    val graph2 = Graph.fromEdgeTuples(edges2)
    
    val sut = new SpectralGraph(graph1)
    assert(sut.lteq(graph1, graph2))
  }
  
  test("lteq_firstGraphSuccedesSecond_returnsFalse"){
    val edges1: RDD[Edge[String]] = sc.parallelize(Array(Edge(0L, 1L, "v"), Edge(0L, 2L, "v"), Edge(0L, 3L, "v"), Edge(1L, 3L, "v"), Edge(3L, 4L, "v")))
    val graph1 = Graph.fromEdgeTuples(edges1)
    
    val edges2: RDD[Edge[String]] = sc.parallelize(Array(Edge(0L, 1L, "v"), Edge(0L, 2L, "v"), Edge(0L, 3L, "v"), Edge(0L, 4L, "v"), Edge(2L, 4L, "v")))
    val graph2 = Graph.fromEdgeTuples(edges2)
    
    val sut = new SpectralGraph(graph1)
    assert(!sut.lteq(graph1, graph2))
  }
  
  test("lteq_equivalentGraphs_returnsTrue"){
    val edges: RDD[Edge[String]] = sc.parallelize(Array(Edge(0L, 1L, "v"), Edge(0L, 2L, "v"), Edge(0L, 3L, "v"), Edge(1L, 3L, "v"), Edge(3L, 4L, "v")))
    val graph = Graph.fromEdgeTuples(edges)
    
    val sut = new SpectralGraph(graph)
    assert(sut.lteq(graph, graph))
  }
}